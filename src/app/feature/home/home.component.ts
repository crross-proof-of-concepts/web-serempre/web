import { Component, OnInit } from '@angular/core';
import { Option as SummaryOption } from '../../shared/summary/summary.component';
import { Option as MenuNavOption } from '../../shared/menu-navbar/menu-navbar.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  summaryOptions: SummaryOption[];
  menuOptions: MenuNavOption[];

  constructor() {}

  ngOnInit() {
    this.summaryOptions = this.buildSummaryOptions();
    this.menuOptions = this.buildMenuNavbar();
  }

  buildSummaryOptions(): SummaryOption[] {
    return [
      {
        value: '42',
        name: 'web design projects'
      },
      {
        value: '123',
        name: 'happy client'
      },
      {
        value: '15',
        name: 'award winner'
      },
      {
        value: '99',
        name: 'cup of coffee'
      },
      {
        value: '24',
        name: 'members'
      }
    ];
  }

  buildMenuNavbar(): MenuNavOption[] {
    return [
      {
        label: 'About',
        link: '/'
      },
      {
        label: 'service',
        link: '/'
      },
      {
        label: 'work',
        link: '/'
      },
      {
        label: 'blog',
        link: '/'
      },
      {
        label: 'contact',
        link: '/'
      }
    ];
  }
}
