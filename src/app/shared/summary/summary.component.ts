import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-summary',
  template: `
    <div class="sh-summary">
      <div *ngFor="let item of items" class="sh-summary__item">
        <span class="sh-summary__item__val">{{ item.value }}</span>
        <span class="sh-summary__item__name">{{ item.name }}</span>
      </div>
    </div>
  `,
  styleUrls: ['./summary.component.scss']
})
export class SummaryComponent {
  @Input() items: Option[];
}

export interface Option {
  value: string;
  name: string;
}
