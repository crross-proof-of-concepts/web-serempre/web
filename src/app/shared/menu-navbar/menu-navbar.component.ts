import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-menu-navbar',
  templateUrl: './menu-navbar.component.html',
  styleUrls: ['./menu-navbar.component.scss']
})
export class MenuNavbarComponent {
  @Input() items: Option[];

  isVisible = false;

  constructor() {}

  onClickBurger() {
    this.isVisible = !this.isVisible;
  }
}

export interface Option {
  label: string;
  link: string;
  isActive?: boolean;
}
