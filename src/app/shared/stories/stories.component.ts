import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-stories',
  templateUrl: './stories.component.html',
  styleUrls: ['./stories.component.scss']
})
export class StoriesComponent implements OnInit {
  @Input() title: string;

  @Input() pretitle: string;

  @Input() description: string;

  @Input() cards: OptionCard[];

  constructor() {}

  ngOnInit() {
    this.cards = this.buildCards();
  }

  buildCards(): OptionCard[] {
    return [
      {
        image: '/assets/Asset 2.png',
        icon: '/assets/Asset 8.svg',
        title: 'Super team'
      },
      {
        image: '/assets/Asset 4.png',
        icon: '/assets/Asset 7.svg',
        title: 'Super team'
      },
      {
        image: '/assets/Asset 5.png',
        icon: '/assets/Asset 6.svg',
        title: 'Super team'
      }
    ];
  }
}

export interface OptionCard {
  image: string;
  icon: string;
  title: string;
}
