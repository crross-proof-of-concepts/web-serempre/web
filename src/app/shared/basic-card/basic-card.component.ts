import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-basic-card',
  templateUrl: './basic-card.component.html',
  styleUrls: ['./basic-card.component.scss']
})
export class BasicCardComponent implements OnInit {
  @Input() image: string;

  @Input() icon: string;

  @Input() title: string;

  style: any;
  constructor() {}

  ngOnInit() {
    this.style = {
      'background-image': 'url("' + this.image + '")'
    };
  }
}
