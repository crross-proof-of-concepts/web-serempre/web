import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BannerComponent } from './banner/banner.component';
import { CoreModule } from '../core/core.module';
import { StoriesComponent } from './stories/stories.component';
import { SummaryComponent } from './summary/summary.component';
import { BasicCardComponent } from './basic-card/basic-card.component';
import { MenuNavbarComponent } from './menu-navbar/menu-navbar.component';

@NgModule({
  declarations: [
    BannerComponent,
    StoriesComponent,
    SummaryComponent,
    BasicCardComponent,
    MenuNavbarComponent
  ],
  imports: [CommonModule, CoreModule],
  exports: [
    BannerComponent,
    StoriesComponent,
    SummaryComponent,
    MenuNavbarComponent
  ]
})
export class SharedModule {}
